//test arikan
//#if defined(UNICODE) && !defined(_UNICODE)
//    #define _UNICODE
//#elif defined(_UNICODE) && !defined(UNICODE)
//    #define UNICODE
//#endif
//
//#include <tchar.h>
//#include <windows.h>
//
///*  Declare Windows procedure  */
//LRESULT CALLBACK WindowProcedure (HWND, UINT, WPARAM, LPARAM);
//
///*  Make the class name into a global variable  */
//TCHAR szClassName[ ] = _T("CodeBlocksWindowsApp");
//
//int WINAPI WinMain (HINSTANCE hThisInstance,
//                     HINSTANCE hPrevInstance,
//                     LPSTR lpszArgument,
//                     int nCmdShow)
//{
//    HWND hwnd;               /* This is the handle for our window */
//    MSG messages;            /* Here messages to the application are saved */
//    WNDCLASSEX wincl;        /* Data structure for the windowclass */
//
//    /* The Window structure */
//    wincl.hInstance = hThisInstance;
//    wincl.lpszClassName = szClassName;
//    wincl.lpfnWndProc = WindowProcedure;      /* This function is called by windows */
//    wincl.style = CS_DBLCLKS;                 /* Catch double-clicks */
//    wincl.cbSize = sizeof (WNDCLASSEX);
//
//    /* Use default icon and mouse-pointer */
//    wincl.hIcon = LoadIcon (NULL, IDI_APPLICATION);
//    wincl.hIconSm = LoadIcon (NULL, IDI_APPLICATION);
//    wincl.hCursor = LoadCursor (NULL, IDC_ARROW);
//    wincl.lpszMenuName = NULL;                 /* No menu */
//    wincl.cbClsExtra = 0;                      /* No extra bytes after the window class */
//    wincl.cbWndExtra = 0;                      /* structure or the window instance */
//    /* Use Windows's default colour as the background of the window */
//    wincl.hbrBackground = (HBRUSH) COLOR_BACKGROUND;
//
//    /* Register the window class, and if it fails quit the program */
//    if (!RegisterClassEx (&wincl))
//        return 0;
//
//    /* The class is registered, let's create the program*/
//    hwnd = CreateWindowEx (
//           0,                   /* Extended possibilites for variation */
//           szClassName,         /* Classname */
//           _T("Code::Blocks Template Windows App"),       /* Title Text */
//           WS_OVERLAPPEDWINDOW, /* default window */
//           CW_USEDEFAULT,       /* Windows decides the position */
//           CW_USEDEFAULT,       /* where the window ends up on the screen */
//           544,                 /* The programs width */
//           375,                 /* and height in pixels */
//           HWND_DESKTOP,        /* The window is a child-window to desktop */
//           NULL,                /* No menu */
//           hThisInstance,       /* Program Instance handler */
//           NULL                 /* No Window Creation data */
//           );
//
//    /* Make the window visible on the screen */
//    ShowWindow (hwnd, nCmdShow);
//
//    /* Run the message loop. It will run until GetMessage() returns 0 */
//    while (GetMessage (&messages, NULL, 0, 0))
//    {
//        /* Translate virtual-key messages into character messages */
//        TranslateMessage(&messages);
//        /* Send message to WindowProcedure */
//        DispatchMessage(&messages);
//    }
//
//    /* The program return-value is 0 - The value that PostQuitMessage() gave */
//    return messages.wParam;
//}
//
//
///*  This function is called by the Windows function DispatchMessage()  */
//
//LRESULT CALLBACK WindowProcedure (HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam)
//{
//    switch (message)                  /* handle the messages */
//    {
//        case WM_DESTROY:
//            PostQuitMessage (0);       /* send a WM_QUIT to the message queue */
//            break;
//        default:                      /* for messages that we don't deal with */
//            return DefWindowProc (hwnd, message, wParam, lParam);
//    }
//
//    return 0;
//}

#include <winsock2.h>
#include <windows.h>
#include <string>
#include <cstring>
#include <curl/curl.h>
#include <iostream>

#define BTN_0  0
#define BTN_1  1
#define BTN_2  2
#define BTN_3  3
#define BTN_4  4
#define BTN_5  5
#define BTN_6  6
#define BTN_7  7
#define BTN_8  8
#define BTN_9  9
#define BTN_S  10
#define BTN_C  11
#define BTN_B  12
#define BTN_H  13
#define EDT_A  14
#define EDT_C  15
using namespace std;
void button_manage(HWND hwnd, int button_code)
{
	char str[6];
	HWND hCodeEdit = GetDlgItem(hwnd, EDT_C);
	SendMessage(hCodeEdit, WM_GETTEXT, (WPARAM)10, (LPARAM)str);
	int ndx = GetWindowTextLength (hCodeEdit);
	char buf[3];
	sprintf(buf,"%d",button_code);
	string s(buf);
	//string s = std::to_string(button_code);

	if ( ndx == 0 )
	{
		//SetFocus (hCodeEdit);

		SendMessage (hCodeEdit, EM_SETSEL, 0, MAKELONG (ndx, ndx));
		SendMessage (hCodeEdit, EM_REPLACESEL, 0,  (LPARAM) (LPSTR) s.c_str() );

	}else if (ndx < 6) {
		char * newArray = new char[std::strlen(str)];
		std::strcpy(newArray,str);
		std::strcat(newArray, s.c_str());
		SetFocus (hCodeEdit);
		SendMessage (hCodeEdit, EM_SETSEL, 0, MAKELONG (ndx, ndx));
		SendMessage (hCodeEdit, EM_REPLACESEL, 0,  (LPARAM) (LPSTR) newArray);
	}else {
	}

		//int outLength = GetWindowTextLength( hCodeEdit ) + 2;
}
void back_space(HWND hwnd)
{

	char str[6];
	HWND hCodeEdit = GetDlgItem(hwnd, EDT_C);
	SendMessage(hCodeEdit, WM_GETTEXT, (WPARAM)10, (LPARAM)str);
	//GetDlgItemText(hwnd, ID_EDITBOX, szBuffer, MAX_PATH);
	int len = strlen(str);
	if (len==1)
	{
//		SendMessage (hCodeEdit, EM_SETSEL, 0, MAKELONG (0, 0));
//		SendMessage (hCodeEdit, EM_REPLACESEL, 0,  (LPARAM) (LPSTR) "");
		SetWindowText(hCodeEdit,"");
	}
	str[len-1]=0;
	SetFocus (hCodeEdit);
	SendMessage (hCodeEdit, EM_SETSEL, 0, MAKELONG (len-1, len-1));
	SendMessage (hCodeEdit, EM_REPLACESEL, 0,  (LPARAM) (LPSTR) str);
	//SetDlgItemText(hwnd,ID_EDITBOX,szBuffer);

}

/* This is where all the input to the window goes to */
void send(HWND hwnd)
{
	char amount[7];
	char code[6];
	HWND hCodeEdit = GetDlgItem(hwnd, EDT_C);
	HWND hAmountEdit = GetDlgItem(hwnd, EDT_A);
	SendMessage(hCodeEdit, WM_GETTEXT, (WPARAM)10, (LPARAM)code);
	SendMessage(hCodeEdit, WM_GETTEXT, (WPARAM)10, (LPARAM)amount);
	if (*amount==NULL || *code==NULL)
		MessageBox(hwnd, "Lutfen Kod ve Turari girin", "Caption", MB_OK);
	else
		{
			MessageBox(hwnd, code, "Caption", MB_OK);
			MessageBox(hwnd, amount, "Caption", MB_OK);
		}

}
LRESULT CALLBACK WndProc(HWND hwnd, UINT Message, WPARAM wParam, LPARAM lParam) {
	switch(Message) {

		/* Upon destruction, tell the main thread to stop */
		case WM_COMMAND:
//			if (LOWORD(wParam) == 1 && HIWORD(wParam) == BN_CLICKED && (HWND)lParam == hCancel)
//				DestroyWindow (hwnd);
			switch(wParam)
			{
				case 0 ... 9:
					button_manage(hwnd, wParam);
					break;
				case BTN_B:
					back_space(hwnd);
					break;
				case BTN_S:
					send(hwnd);
					break;
				case BTN_C:
					DestroyWindow(hwnd);
      				break;
			}

			break;
		case WM_LBUTTONDOWN:	// <-
                            	// <-     we just added this stuff
      		break;
		case WM_CLOSE:
        	DestroyWindow(hwnd);
      		break;
		case WM_DESTROY: {
			PostQuitMessage(0);
			break;
		}

		/* All other messages (a lot of them) are processed using default procedures */
		default:
			return DefWindowProc(hwnd, Message, wParam, lParam);
	}
	return 0;
}


/* The 'main' function of Win32 GUI programs: this is where execution starts */
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow) {
	CURL *curl;
  	CURLcode res;
	curl_global_init(CURL_GLOBAL_ALL);
	curl = curl_easy_init();
	curl_easy_cleanup(curl);
	curl_global_cleanup();

	WNDCLASSEX wc; /* A properties struct of our window */
	HWND hwnd; /* A 'HANDLE', hence the H, or a pointer to our window */
	HWND hTECode, hTEAmount, hNum1, hNum2, hNum3, hNum4, hNum5, hNum6, hNum7, hNum8, hNum9, hSend, hNum0, hCancel, hBack;
	MSG msg; /* A temporary location for all messages */

	/* zero out the struct and set the stuff we want to modify */
	memset(&wc,0,sizeof(wc));
	wc.cbSize		 = sizeof(WNDCLASSEX);
	wc.lpfnWndProc	 = WndProc; /* This is where we will send messages to */
	wc.hInstance	 = hInstance;
	wc.hCursor		 = LoadCursor(NULL, IDC_ARROW);

	/* White, COLOR_WINDOW is just a #define for a system color, try Ctrl+Clicking it */
	wc.hbrBackground = (HBRUSH)(COLOR_WINDOW);
	wc.lpszClassName = "WindowClass";
	wc.hIcon		 = LoadIcon(NULL, IDI_APPLICATION); /* Load a standard icon */
	wc.hIconSm		 = LoadIcon(NULL, IDI_APPLICATION); /* use the name "A" to use the project icon */

	if(!RegisterClassEx(&wc)) {
		MessageBox(NULL, "Window Registration Failed!","Error!",MB_ICONEXCLAMATION|MB_OK);
		return 0;
	}
	hwnd = CreateWindow("WindowClass","Viatori PoS",WS_VISIBLE | WS_OVERLAPPED | WS_CAPTION,	CW_USEDEFAULT, CW_USEDEFAULT, 197, 380, NULL,NULL,hInstance,NULL);

	//hwnd = CreateWindow("WindowClass","Caption", WS_VISIBLE|WS_OVERLAPPEDWINDOW,	CW_USEDEFAULT, CW_USEDEFAULT, 197, 310, NULL,NULL,hInstance,NULL);

	//SetDlgItemText

	hTECode = CreateWindowEx(WS_EX_CLIENTEDGE, TEXT("EDIT"), TEXT(""), WS_CHILD|WS_VISIBLE|ES_AUTOHSCROLL|WS_TABSTOP|ES_RIGHT, 5, 5, 90, 35, hwnd, (HMENU)EDT_C, GetModuleHandle(NULL), NULL);
	hTEAmount = CreateWindowEx(WS_EX_CLIENTEDGE, TEXT("EDIT"), TEXT(""), WS_CHILD|WS_VISIBLE|ES_AUTOHSCROLL|WS_TABSTOP|ES_RIGHT, 96, 5, 89, 35, hwnd, (HMENU)EDT_A, GetModuleHandle(NULL), NULL);

	hNum1 = CreateWindowEx(WS_EX_CLIENTEDGE, TEXT("BUTTON"),TEXT("1"),  WS_TABSTOP | WS_VISIBLE | WS_CHILD | BS_DEFPUSHBUTTON, 5, 45, 60, 60, hwnd, (HMENU)BTN_1,  GetModuleHandle(NULL), NULL);
	hNum2 = CreateWindowEx(WS_EX_CLIENTEDGE, TEXT("BUTTON"),TEXT("2"),  WS_TABSTOP | WS_VISIBLE | WS_CHILD | BS_DEFPUSHBUTTON, 65, 45, 60, 60, hwnd, (HMENU)BTN_2,  GetModuleHandle(NULL), NULL);
	hNum3 = CreateWindowEx(WS_EX_CLIENTEDGE, TEXT("BUTTON"),TEXT("3"),  WS_TABSTOP | WS_VISIBLE | WS_CHILD | BS_DEFPUSHBUTTON, 125, 45, 60, 60, hwnd, (HMENU)BTN_3,  GetModuleHandle(NULL), NULL);

	hNum4 = CreateWindowEx(WS_EX_CLIENTEDGE, TEXT("BUTTON"),TEXT("4"),  WS_TABSTOP | WS_VISIBLE | WS_CHILD | BS_DEFPUSHBUTTON, 5, 105, 60, 60, hwnd, (HMENU)BTN_4,  GetModuleHandle(NULL), NULL);
	hNum5 = CreateWindowEx(WS_EX_CLIENTEDGE, TEXT("BUTTON"),TEXT("5"),  WS_TABSTOP | WS_VISIBLE | WS_CHILD | BS_DEFPUSHBUTTON, 65, 105, 60, 60, hwnd, (HMENU)BTN_5,  GetModuleHandle(NULL), NULL);
	hNum6 = CreateWindowEx(WS_EX_CLIENTEDGE, TEXT("BUTTON"),TEXT("6"),  WS_TABSTOP | WS_VISIBLE | WS_CHILD | BS_DEFPUSHBUTTON, 125, 105, 60, 60, hwnd, (HMENU)BTN_6,  GetModuleHandle(NULL), NULL);

	hNum7 = CreateWindowEx(WS_EX_CLIENTEDGE, TEXT("BUTTON"),TEXT("7"),  WS_TABSTOP | WS_VISIBLE | WS_CHILD | BS_DEFPUSHBUTTON, 5, 165, 60, 60, hwnd, (HMENU)BTN_7,  GetModuleHandle(NULL), NULL);
	hNum8 = CreateWindowEx(WS_EX_CLIENTEDGE, TEXT("BUTTON"),TEXT("8"),  WS_TABSTOP | WS_VISIBLE | WS_CHILD | BS_DEFPUSHBUTTON, 65, 165, 60, 60, hwnd, (HMENU)BTN_8,  GetModuleHandle(NULL), NULL);
	hNum9 = CreateWindowEx(WS_EX_CLIENTEDGE, TEXT("BUTTON"),TEXT("9"),  WS_TABSTOP | WS_VISIBLE | WS_CHILD | BS_DEFPUSHBUTTON, 125, 165, 60, 60, hwnd, (HMENU)BTN_9,  GetModuleHandle(NULL), NULL);

	hSend = CreateWindowEx(WS_EX_CLIENTEDGE, TEXT("BUTTON"),TEXT("Gonder"),  WS_TABSTOP | WS_VISIBLE | WS_CHILD | BS_DEFPUSHBUTTON,  5, 225, 60, 60, hwnd, (HMENU)BTN_S,  GetModuleHandle(NULL), NULL);
	hNum0 = CreateWindowEx(WS_EX_CLIENTEDGE, TEXT("BUTTON"),TEXT("0"),  WS_TABSTOP | WS_VISIBLE | WS_CHILD | BS_DEFPUSHBUTTON,       65, 225, 60, 60, hwnd, (HMENU)BTN_0,  GetModuleHandle(NULL), NULL);
	hCancel = CreateWindowEx(WS_EX_CLIENTEDGE, TEXT("BUTTON"),TEXT("Iptal"),  WS_TABSTOP | WS_VISIBLE | WS_CHILD | BS_DEFPUSHBUTTON, 125, 225, 60, 60, hwnd, (HMENU)BTN_C,  GetModuleHandle(NULL), NULL);

	hBack = CreateWindowEx(WS_EX_CLIENTEDGE, TEXT("BUTTON"),TEXT("Sil"),  WS_TABSTOP | WS_VISIBLE | WS_CHILD | BS_DEFPUSHBUTTON, 5, 280, 60, 60, hwnd, (HMENU)BTN_B,  GetModuleHandle(NULL), NULL);
	hBack = CreateWindowEx(WS_EX_CLIENTEDGE, TEXT("BUTTON"),TEXT("ABC"),  WS_TABSTOP | WS_VISIBLE | WS_CHILD | BS_DEFPUSHBUTTON, 65, 280, 60, 60, hwnd, (HMENU)BTN_H,  GetModuleHandle(NULL), NULL);
	SendMessage(hTECode, EM_SETLIMITTEXT, 6, 0);
	SendMessage(hTEAmount, EM_SETLIMITTEXT, 7, 0);

	if(hwnd == NULL) {
		MessageBox(NULL, "Window Creation Failed!","Error!",MB_ICONEXCLAMATION|MB_OK);
		return 0;
	}

	/*
		This is the heart of our program where all input is processed and
		sent to WndProc. Note that GetMessage blocks code flow until it receives something, so
		this loop will not produce unreasonably high CPU usage
	*/
	while(GetMessage(&msg, NULL, 0, 0) > 0) { /* If no error is received... */
		TranslateMessage(&msg); /* Translate key codes to chars if present */
		DispatchMessage(&msg); /* Send it to WndProc */
	}
	return msg.wParam;
}

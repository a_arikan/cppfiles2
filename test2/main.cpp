#include <tchar.h>
#include <winsock2.h>
#include <windows.h>
#include <string>
#include <cstring>
#include <curl/curl.h>
#include <iostream>
#include <sstream>
#include <openssl/ssl.h>
#include <json/json.h>

#define BTN_0  0
#define BTN_1  1
#define BTN_2  2
#define BTN_3  3
#define BTN_4  4
#define BTN_5  5
#define BTN_6  6
#define BTN_7  7
#define BTN_8  8
#define BTN_9  9
#define BTN_00 10
#define BTN_S  11
#define BTN_C  12
#define BTN_B  13
#define BTN_H  14
#define BTN_D  15
#define BTN_L  16
#define BTN_W  17
#define BTN_M  18
#define LBL_C  19
#define LBL_A  20
#define EDT_A  21
#define EDT_C  22

HWND hLAAmount, settingstext, hCodeEdit, hAmountEdit, hTECode, hTEAmount, hNum1, hNum2, hNum3, hNum4, hNum5, hNum6, hNum7, hNum8, hNum9, hSend, hNum0, hCancel, hBack, hLett, hDot, hNum00, hWhole, hMinMax, hLACode;
LPCTSTR ClsName = "GDIFund";
LPCTSTR WindowCaption = "GDI Fundamentals";
static HBRUSH hBrush = CreateSolidBrush(RGB(230,230,230));
//static CURLcode sslctx_function(CURL *curl, void *sslctx, void *parm)
//{
//  X509_STORE *store;
//  X509 *cert=NULL;
//  BIO *bio;
//  const char *mypem =\
//    "-----BEGIN CERTIFICATE-----\n"\
//    "MIIHPTCCBSWgAwIBAgIBADANBgkqhkiG9w0BAQQFADB5MRAwDgYDVQQKEwdSb290\n"\
//    "IENBMR4wHAYDVQQLExVodHRwOi8vd3d3LmNhY2VydC5vcmcxIjAgBgNVBAMTGUNB\n"\
//    "IENlcnQgU2lnbmluZyBBdXRob3JpdHkxITAfBgkqhkiG9w0BCQEWEnN1cHBvcnRA\n"\
//    "Y2FjZXJ0Lm9yZzAeFw0wMzAzMzAxMjI5NDlaFw0zMzAzMjkxMjI5NDlaMHkxEDAO\n"\
//    "BgNVBAoTB1Jvb3QgQ0ExHjAcBgNVBAsTFWh0dHA6Ly93d3cuY2FjZXJ0Lm9yZzEi\n"\
//    "MCAGA1UEAxMZQ0EgQ2VydCBTaWduaW5nIEF1dGhvcml0eTEhMB8GCSqGSIb3DQEJ\n"\
//    "ARYSc3VwcG9ydEBjYWNlcnQub3JnMIICIjANBgkqhkiG9w0BAQEFAAOCAg8AMIIC\n"\
//    "CgKCAgEAziLA4kZ97DYoB1CW8qAzQIxL8TtmPzHlawI229Z89vGIj053NgVBlfkJ\n"\
//    "8BLPRoZzYLdufujAWGSuzbCtRRcMY/pnCujW0r8+55jE8Ez64AO7NV1sId6eINm6\n"\
//    "zWYyN3L69wj1x81YyY7nDl7qPv4coRQKFWyGhFtkZip6qUtTefWIonvuLwphK42y\n"\
//    "fk1WpRPs6tqSnqxEQR5YYGUFZvjARL3LlPdCfgv3ZWiYUQXw8wWRBB0bF4LsyFe7\n"\
//    "w2t6iPGwcswlWyCR7BYCEo8y6RcYSNDHBS4CMEK4JZwFaz+qOqfrU0j36NK2B5jc\n"\
//    "G8Y0f3/JHIJ6BVgrCFvzOKKrF11myZjXnhCLotLddJr3cQxyYN/Nb5gznZY0dj4k\n"\
//    "epKwDpUeb+agRThHqtdB7Uq3EvbXG4OKDy7YCbZZ16oE/9KTfWgu3YtLq1i6L43q\n"\
//    "laegw1SJpfvbi1EinbLDvhG+LJGGi5Z4rSDTii8aP8bQUWWHIbEZAWV/RRyH9XzQ\n"\
//    "QUxPKZgh/TMfdQwEUfoZd9vUFBzugcMd9Zi3aQaRIt0AUMyBMawSB3s42mhb5ivU\n"\
//    "fslfrejrckzzAeVLIL+aplfKkQABi6F1ITe1Yw1nPkZPcCBnzsXWWdsC4PDSy826\n"\
//    "YreQQejdIOQpvGQpQsgi3Hia/0PsmBsJUUtaWsJx8cTLc6nloQsCAwEAAaOCAc4w\n"\
//    "ggHKMB0GA1UdDgQWBBQWtTIb1Mfz4OaO873SsDrusjkY0TCBowYDVR0jBIGbMIGY\n"\
//    "gBQWtTIb1Mfz4OaO873SsDrusjkY0aF9pHsweTEQMA4GA1UEChMHUm9vdCBDQTEe\n"\
//    "MBwGA1UECxMVaHR0cDovL3d3dy5jYWNlcnQub3JnMSIwIAYDVQQDExlDQSBDZXJ0\n"\
//    "IFNpZ25pbmcgQXV0aG9yaXR5MSEwHwYJKoZIhvcNAQkBFhJzdXBwb3J0QGNhY2Vy\n"\
//    "dC5vcmeCAQAwDwYDVR0TAQH/BAUwAwEB/zAyBgNVHR8EKzApMCegJaAjhiFodHRw\n"\
//    "czovL3d3dy5jYWNlcnQub3JnL3Jldm9rZS5jcmwwMAYJYIZIAYb4QgEEBCMWIWh0\n"\
//    "dHBzOi8vd3d3LmNhY2VydC5vcmcvcmV2b2tlLmNybDA0BglghkgBhvhCAQgEJxYl\n"\
//    "aHR0cDovL3d3dy5jYWNlcnQub3JnL2luZGV4LnBocD9pZD0xMDBWBglghkgBhvhC\n"\
//    "AQ0ESRZHVG8gZ2V0IHlvdXIgb3duIGNlcnRpZmljYXRlIGZvciBGUkVFIGhlYWQg\n"\
//    "b3ZlciB0byBodHRwOi8vd3d3LmNhY2VydC5vcmcwDQYJKoZIhvcNAQEEBQADggIB\n"\
//    "ACjH7pyCArpcgBLKNQodgW+JapnM8mgPf6fhjViVPr3yBsOQWqy1YPaZQwGjiHCc\n"\
//    "nWKdpIevZ1gNMDY75q1I08t0AoZxPuIrA2jxNGJARjtT6ij0rPtmlVOKTV39O9lg\n"\
//    "18p5aTuxZZKmxoGCXJzN600BiqXfEVWqFcofN8CCmHBh22p8lqOOLlQ+TyGpkO/c\n"\
//    "gr/c6EWtTZBzCDyUZbAEmXZ/4rzCahWqlwQ3JNgelE5tDlG+1sSPypZt90Pf6DBl\n"\
//    "Jzt7u0NDY8RD97LsaMzhGY4i+5jhe1o+ATc7iwiwovOVThrLm82asduycPAtStvY\n"\
//    "sONvRUgzEv/+PDIqVPfE94rwiCPCR/5kenHA0R6mY7AHfqQv0wGP3J8rtsYIqQ+T\n"\
//    "SCX8Ev2fQtzzxD72V7DX3WnRBnc0CkvSyqD/HMaMyRa+xMwyN2hzXwj7UfdJUzYF\n"\
//    "CpUCTPJ5GhD22Dp1nPMd8aINcGeGG7MW9S/lpOt5hvk9C8JzC6WZrG/8Z7jlLwum\n"\
//    "GCSNe9FINSkYQKyTYOGWhlC0elnYjyELn8+CkcY7v2vcB5G5l1YjqrZslMZIBjzk\n"\
//    "zk6q5PYvCdxTby78dOs6Y5nCpqyJvKeyRKANihDjbPIky/qbn3BHLt4Ui9SyIAmW\n"\
//    "omTxJBzcoTWcFbLUvFUufQb1nA5V9FrWk9p2rSVzTMVD\n"\
//    "-----END CERTIFICATE-----\n";
//  /* get a BIO */
//  bio=BIO_new_mem_buf(mypem, -1);
//  /* use it to read the PEM formatted certificate from memory into an X509
//   * structure that SSL can use
//   */
//  PEM_read_bio_X509(bio, &cert, 0, NULL);
//  if(cert == NULL)
//    printf("PEM_read_bio_X509 failed...\n");
//
//  /* get a pointer to the X509 certificate store (which may be empty!) */
//  store=SSL_CTX_get_cert_store((SSL_CTX *)sslctx);
//
//  /* add our certificate to this store */
//  if(X509_STORE_add_cert(store, cert)==0)
//    printf("error adding certificate\n");
//
//  /* decrease reference counts */
//  X509_free(cert);
//  BIO_free(bio);
//
//  /* all set to go */
//  return CURLE_OK;
//}
//

std::wstring get_utf16(const std::string &str)
{
    if (str.empty()) return std::wstring();
    int sz = MultiByteToWideChar(CP_UTF8, 0, &str[0], (int)str.size(), 0, 0);
    std::wstring res(sz, 0);
    MultiByteToWideChar(CP_UTF8, 0, &str[0], (int)str.size(), &res[0], sz);
    return res;
}

static size_t WriteCallback(void *contents, size_t size, size_t nmemb, void *userp)
{
    ((std::string*)userp)->append((char*)contents, size * nmemb);
    return size * nmemb;
}
void button_manage(HWND hwnd, int button_code, HWND button)
{
	char str[6];
	HWND hCodeEdit = button;
	SendMessage(hCodeEdit, WM_GETTEXT, (WPARAM)10, (LPARAM)str);
	int ndx = GetWindowTextLength (hCodeEdit);
	std::stringstream s;
	s << button_code;
	//std::string s = std::to_string(button_code);
	if ( ndx == 0 )
	{
		//SetFocus (hCodeEdit);

		SendMessage (hCodeEdit, EM_SETSEL, 0, MAKELONG (ndx, ndx));
		SendMessage (hCodeEdit, EM_REPLACESEL, 0,  (LPARAM) (LPSTR) s.str().c_str() );

	}else if (ndx < 6) {
		char * newArray = new char[std::strlen(str)];
		std::strcpy(newArray,str);
		std::strcat(newArray, s.str().c_str());
		SetFocus (hCodeEdit);
		SendMessage (hCodeEdit, EM_SETSEL, 0, MAKELONG (ndx, ndx));
		SendMessage (hCodeEdit, EM_REPLACESEL, 0,  (LPARAM) (LPSTR) newArray);
	}else {
	}

		//int outLength = GetWindowTextLength( hCodeEdit ) + 2;
}
void back_space(HWND hwnd, HWND button)
{

	char str[6];
	//HWND hCodeEdit = GetDlgItem(hwnd, EDT_C);
	HWND hCodeEdit = button;
	SendMessage(hCodeEdit, WM_GETTEXT, (WPARAM)10, (LPARAM)str);
	//GetDlgItemText(hwnd, ID_EDITBOX, szBuffer, MAX_PATH);
	int len = strlen(str);
	if (len==1)
	{
//		SendMessage (hCodeEdit, EM_SETSEL, 0, MAKELONG (0, 0));
//		SendMessage (hCodeEdit, EM_REPLACESEL, 0,  (LPARAM) (LPSTR) "");
		SetWindowText(hCodeEdit,"");
	}
	str[len-1]=0;
	SetFocus (hCodeEdit);
	SendMessage (hCodeEdit, EM_SETSEL, 0, MAKELONG (len-1, len-1));
	SendMessage (hCodeEdit, EM_REPLACESEL, 0,  (LPARAM) (LPSTR) str);
	//SetDlgItemText(hwnd,ID_EDITBOX,szBuffer);

}
void button_dot(HWND hwnd, HWND button)
{

	char str[6];
	HWND hCodeEdit = button;
	SendMessage(hCodeEdit, WM_GETTEXT, (WPARAM)10, (LPARAM)str);
	int ndx = GetWindowTextLength (hCodeEdit);
	std::string s = ".";
	if ( ndx == 0 )
	{
		//SetFocus (hCodeEdit);

		SendMessage (hCodeEdit, EM_SETSEL, 0, MAKELONG (ndx, ndx));
		SendMessage (hCodeEdit, EM_REPLACESEL, 0,  (LPARAM) (LPSTR) s.c_str() );

	}else if (ndx < 6) {
		if (!std::strchr(str,'.'))
		{
			char * newArray = new char[std::strlen(str)];
			std::strcpy(newArray,str);
			std::strcat(newArray, s.c_str());
			SetFocus (hCodeEdit);
			SendMessage (hCodeEdit, EM_SETSEL, 0, MAKELONG (ndx, ndx));
			SendMessage (hCodeEdit, EM_REPLACESEL, 0,  (LPARAM) (LPSTR) newArray);
		}

	}else {
	}

		//int outLength = GetWindowTextLength( hCodeEdit ) + 2;

}
void button_dz(HWND hwnd, HWND button)
{

	char str[6];
	HWND hCodeEdit = button;
	SendMessage(hCodeEdit, WM_GETTEXT, (WPARAM)10, (LPARAM)str);
	int ndx = GetWindowTextLength (hCodeEdit);
	std::string s = "00";
	if ( ndx == 0 )
	{
		//SetFocus (hCodeEdit);

		SendMessage (hCodeEdit, EM_SETSEL, 0, MAKELONG (ndx, ndx));
		SendMessage (hCodeEdit, EM_REPLACESEL, 0,  (LPARAM) (LPSTR) s.c_str() );

	}else if (ndx < 6) {
		char * newArray = new char[std::strlen(str)];
		std::strcpy(newArray,str);
		std::strcat(newArray, s.c_str());
		SetFocus (hCodeEdit);
		SendMessage (hCodeEdit, EM_SETSEL, 0, MAKELONG (ndx, ndx));
		SendMessage (hCodeEdit, EM_REPLACESEL, 0,  (LPARAM) (LPSTR) newArray);
	}else {
	}

		//int outLength = GetWindowTextLength( hCodeEdit ) + 2;

}
void whole(HWND hwnd)
{
//	std::string amount;
//	std::string code;
	char amount[7];
	char code[6];
	//double code;
    hCodeEdit = GetDlgItem(hwnd, EDT_C);
	hAmountEdit = GetDlgItem(hwnd, EDT_A);
	//SendMessage(hCodeEdit, WM_GETTEXT, (WPARAM), reinterpret_cast< WPARAM &>(code));
//	GetWindowText(hCodeEdit,code,8);
//	GetWindowText(hAmountEdit,amount,9);
	SendMessage(hCodeEdit, WM_GETTEXT, (WPARAM)7, (LPARAM)code);
	std::string c(code);
	SendMessage(hAmountEdit, WM_GETTEXT, (WPARAM)8, (LPARAM)amount);
	std::string a(amount);
//	int a = amount.length();
//	int b = code.length();
	//if (!amount.empty() || !code.empty())
	if (*amount=='\0' || *code=='\0')
		MessageBox(hwnd, "Lutfen Kod ve Turari girin", "Caption", MB_OK);
	else
		{
			//MessageBox(hwnd, c.c_str(), "Caption", MB_OK);
			//MessageBox(hwnd, a.c_str(), "Caption", MB_OK);

//			double  = reinterpret_cast< WPARAM &>(code);

			SendMessage(hwnd,WM_SETTEXT,0,(LPARAM)"Kod Gonderiliyor ...");
//			std::string s2 (static_cast<const char *>(code));
//			std::string test = std::string(code) + "  d  " + std::string(amount);
//			std::stringstream link;
//			link << "discount_code=" << code << "&amount=" << amount << "&branch_id=102" << std::endl;
//			link.append(code);
//			link.append("&amount=");
//			link.append(amount);
//			link.append("&branch_id=102");
//			= "discount_code=" + code + "&amount=" + amount + "&branch_id=102";
//			link.append("discount_code=");
//			std::string s1 = link.str();
			std::string postthis = "discount_code=" + c + "&amount=" + a + "&branch_id=1";
//			char postthis[strlen("discount_code=") + strlen("&amount=") + strlen("&branch_id=102") + strlen(code) + strlen(amount) + 1];
//			strcpy(postthis,"discount_code=");
//			strcat(postthis, code);
//			strcat(postthis, "&amount=");
//			strcat(postthis, amount);
//			strcat(postthis, "&branch_id=102");

//			static const char *postthis="discount_code=795567&amount=10000&branch_id=102";
//			static const char *postthis;
//			static const char *postthis="user_id=1300318&token=b49db5b0-34fb-11e7-89c3-df8e5fc2df";
			CURL *curl;
  			CURLcode res;
			std::string readBuffer;
			curl_global_init(CURL_GLOBAL_ALL);
			curl = curl_easy_init();
//			curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER , 1);
//    		curl_easy_setopt(curl, CURLOPT_SSL_VERIFYHOST , 1);
//    		curl_easy_setopt(curl, CURLOPT_CAINFO , "curl-ca-bundle.crt");
//			curl_easy_setopt(curl, CURLOPT_URL, "https://lssocial.viatoriapp.com/viatorilumen/public/index.php/api/v1/timeline");
//			curl_easy_setopt(curl, CURLOPT_URL, "http://192.168.0.44/viatori_lumen/public/index.php/api/v1/pos_check");
//			curl_easy_setopt(curl, CURLOPT_URL, "http://212.68.41.92/viatori_lumen/public/index.php/api/v1/pos_check");
//			curl_easy_setopt(curl, CURLOPT_URL, "http://212.68.41.92//viatorilumen/public/index.php/api/v1/checkLastStepForSales");
			curl_easy_setopt(curl, CURLOPT_URL, "http://lssocial.viatoriapp.com/viatorilumen/public/index.php/api/v1/checkLastStepForSales");
			//curl_easy_setopt(curl, CURLOPT_POSTFIELDSIZE, (long)strlen(postthis));
			curl_easy_setopt(curl, CURLOPT_POSTFIELDSIZE, postthis.length());
			curl_easy_setopt(curl, CURLOPT_POSTFIELDS, postthis.c_str());
			//curl_easy_setopt(curl, CURLOPT_POSTFIELDS, "user_id=1300318&token=b49db5b0-34fb-11e7-89c3-df8e5fc2df");

			//curl_easy_setopt(curl, CURLOPT_POSTFIELDSIZE, *postthis);
			curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, WriteCallback);
			curl_easy_setopt(curl, CURLOPT_WRITEDATA, &readBuffer);
			res = curl_easy_perform(curl);
			SetWindowText(hwnd,"Kod Gonderiliyor ...");
			EnableWindow(hwnd, FALSE);
			while(res);
			EnableWindow(hwnd, TRUE);
			SendMessage(hwnd,WM_SETTEXT,0,(LPARAM)"Viatori PoS");
			if(res != CURLE_OK)
      			MessageBox(hwnd, "Bir hata olustu Lutfen Tekrar Deneyin ", "Caption", MB_OK);
				fprintf(stderr, "curl_easy_perform() failed: %s\n", curl_easy_strerror(res));
    		/* always cleanup */

    		//MessageBox(hwnd, readBuffer.c_str(), "Caption", MB_OK);
			Json::Reader root;
			Json::Value obj;
			root.parse(readBuffer, obj);
//			char str[40];
//			std::strcat(str," Status Code: ");
//			std::strcat(str, obj["statusCode"].asString().c_str());
//			if (obj["statusCode"].asInt()==200)
//			std::string msg = obj["errorMessage"].asString();
			std::wstring msg = get_utf16(obj["errorMessage"].asString());
			std::string status = obj["statusCode"].asString();
//          MessageBox(hwnd, obj["errorMessage"].asString().c_str(), "Viatori PoS", MB_OK);
            MessageBoxW(hwnd, msg.c_str(), L"Viatori PoS", MB_OK);
			if (status.compare("400") == 0)
            {
                EnableWindow(hWhole, false);
            }



//			else if(obj["statusCode"].asInt()==405)
    		curl_easy_cleanup(curl);
			curl_global_cleanup();
		}


}
void send(HWND hwnd)
{
//	std::string amount;
//	std::string code;
	char amount[7];
	char code[6];
	//double code;
	hCodeEdit = GetDlgItem(hwnd, EDT_C);
	hAmountEdit = GetDlgItem(hwnd, EDT_A);
	//SendMessage(hCodeEdit, WM_GETTEXT, (WPARAM), reinterpret_cast< WPARAM &>(code));
//	GetWindowText(hCodeEdit,code,8);
//	GetWindowText(hAmountEdit,amount,9);
	SendMessage(hCodeEdit, WM_GETTEXT, (WPARAM)7, (LPARAM)code);
	std::string c(code);
	SendMessage(hAmountEdit, WM_GETTEXT, (WPARAM)8, (LPARAM)amount);
	std::string a(amount);
//	int a = amount.length();
//	int b = code.length();
	//if (!amount.empty() || !code.empty())
	if (*amount=='\0' || *code=='\0')
		MessageBox(hwnd, "Lutfen Kod ve Turari girin", "Caption", MB_OK);
	else
		{
			//MessageBox(hwnd, c.c_str(), "Caption", MB_OK);
			//MessageBox(hwnd, a.c_str(), "Caption", MB_OK);

//			double  = reinterpret_cast< WPARAM &>(code);

			SendMessage(hwnd,WM_SETTEXT,0,(LPARAM)"Kod Gonderiliyor ...");
//			std::string s2 (static_cast<const char *>(code));
//			std::string test = std::string(code) + "  d  " + std::string(amount);
//			std::stringstream link;
//			link << "discount_code=" << code << "&amount=" << amount << "&branch_id=102" << std::endl;
//			link.append(code);
//			link.append("&amount=");
//			link.append(amount);
//			link.append("&branch_id=102");
//			= "discount_code=" + code + "&amount=" + amount + "&branch_id=102";
//			link.append("discount_code=");
//			std::string s1 = link.str();
			std::string postthis = "discount_code=" + c + "&amount=" + a + "&branch_id=1";
//			char postthis[strlen("discount_code=") + strlen("&amount=") + strlen("&branch_id=102") + strlen(code) + strlen(amount) + 1];
//			strcpy(postthis,"discount_code=");
//			strcat(postthis, code);
//			strcat(postthis, "&amount=");
//			strcat(postthis, amount);
//			strcat(postthis, "&branch_id=102");

//			static const char *postthis="discount_code=795567&amount=10000&branch_id=102";
//			static const char *postthis;
//			static const char *postthis="user_id=1300318&token=b49db5b0-34fb-11e7-89c3-df8e5fc2df";
			CURL *curl;
  			CURLcode res;
            std::string readBuffer;
			curl_global_init(CURL_GLOBAL_ALL);
			curl = curl_easy_init();
//			curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER , 1);
//    		curl_easy_setopt(curl, CURLOPT_SSL_VERIFYHOST , 1);
//    		curl_easy_setopt(curl, CURLOPT_CAINFO , "curl-ca-bundle.crt");
//			curl_easy_setopt(curl, CURLOPT_URL, "https://lssocial.viatoriapp.com/viatorilumen/public/index.php/api/v1/timeline");
//			curl_easy_setopt(curl, CURLOPT_URL, "http://192.168.0.44/viatori_lumen/public/index.php/api/v1/pos_check");
//			curl_easy_setopt(curl, CURLOPT_URL, "http://212.68.41.92/viatori_lumen/public/index.php/api/v1/pos_check");
//          curl_easy_setopt(curl, CURLOPT_URL, "http://212.68.41.92//viatorilumen/public/index.php/api/v1/checkBranchCodeOnPosSystem");
			curl_easy_setopt(curl, CURLOPT_URL, "http://lssocial.viatoriapp.com/viatorilumen/public/index.php/api/v1/checkBranchCodeOnPosSystem");
			//curl_easy_setopt(curl, CURLOPT_POSTFIELDSIZE, (long)strlen(postthis));
			curl_easy_setopt(curl, CURLOPT_POSTFIELDSIZE, postthis.length());
			curl_easy_setopt(curl, CURLOPT_POSTFIELDS, postthis.c_str());
			//curl_easy_setopt(curl, CURLOPT_POSTFIELDS, "user_id=1300318&token=b49db5b0-34fb-11e7-89c3-df8e5fc2df");

			//curl_easy_setopt(curl, CURLOPT_POSTFIELDSIZE, *postthis);
			curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, WriteCallback);
			curl_easy_setopt(curl, CURLOPT_WRITEDATA, &readBuffer);
			res = curl_easy_perform(curl);
			SetWindowText(hwnd,"Kod Gonderiliyor ...");
			EnableWindow(hwnd, FALSE);
			while(res);
			EnableWindow(hwnd, TRUE);
			SendMessage(hwnd,WM_SETTEXT,0,(LPARAM)"Viatori PoS");
			if(res != CURLE_OK)
      			MessageBox(hwnd, "Bir hata olustu Lutfen Tekrar Deneyin ", "Caption", MB_OK);
				fprintf(stderr, "curl_easy_perform() failed: %s\n", curl_easy_strerror(res));
    		/* always cleanup */

    		//MessageBox(hwnd, readBuffer.c_str(), "Caption", MB_OK);
			Json::Reader root;
			Json::Value obj;
			root.parse(readBuffer, obj);
//			char str[40];
//			std::strcat(str," Status Code: ");
//			std::strcat(str, obj["statusCode"].asString().c_str());
//			if (obj["statusCode"].asInt()==200)
			std::wstring msg = get_utf16(obj["errorMessage"].asString());
			std::string status = obj["statusCode"].asString();
			//std::wstring wmshg = get_utf16(msg);
            MessageBoxW(hwnd, msg.c_str(), L"Viatori PoS", MB_OK);
            //MessageBox(hwnd, obj["errorMessage"].asString().c_str(), "Viatori PoS", MB_OK);
			if (status.compare("400") == 0 || status.compare("405"))
            {
                EnableWindow(hWhole, true);
            }


//			else if(obj["statusCode"].asInt()==405)
    		curl_easy_cleanup(curl);
			curl_global_cleanup();
		}


}
LRESULT CALLBACK WndProc(HWND hwnd, UINT Message, WPARAM wParam, LPARAM lParam) /* This is where all the input to the window goes to */
{
	switch(Message) {

		/* Upon destruction, tell the main thread to stop */
		case WM_COMMAND:
//			if (LOWORD(wParam) == 1 && HIWORD(wParam) == BN_CLICKED && (HWND)lParam == hCancel)
//				DestroyWindow (hwnd);
			//GetFocus()
//			HWND hCodeEdit = GetDlgItem(hwnd, EDT_C);

			//MessageBox(hwnd, HIWORD(wParam), "Oldu Bu cok guzel oldu :) ", MB_OK);
//			int hi;
//			hi = HIWORD(wParam);
//			int lo;
//			lo = LOWORD(wParam);

			if((HIWORD(wParam) == 256) && (LOWORD(wParam) == EDT_A))
		 	{
		 	//	MessageBox(hwnd, "Tutar", "Oldu Bu cok guzel oldu :) ", MB_OK);
		 	hCodeEdit = GetDlgItem(hwnd, EDT_A);
			}

			if((HIWORD(wParam) == 256) && (LOWORD(wParam) == EDT_C))
		 	{
		 	//	MessageBox(hwnd, "Kod", "Oldu Bu cok guzel oldu :) ", MB_OK);
		 	hCodeEdit = GetDlgItem(hwnd, EDT_C);
			}



			switch(wParam)
			{
				case 0 ... 9:
					button_manage(hwnd, wParam, hCodeEdit);
					break;
				case BTN_D:
					button_dot(hwnd,hCodeEdit);
					break;
				case BTN_B:
					back_space(hwnd, hCodeEdit);
					break;
				case BTN_S:
					send(hwnd);
					break;
				case BTN_00:
					button_dz(hwnd,hCodeEdit);
					break;
				case BTN_W:
                    whole(hwnd);
                    break;
				case BTN_C:
					DestroyWindow(hwnd);
      				break;
			}

			break;
		case WM_LBUTTONDOWN:	// <-
      		break;

//        case WM_CTLCOLORSTATIC:
//        {
//            if ( settingstext == (hwnd) lParam )
//                DWORD CtrlID = GetDlgCtrlID((HWND)lParam);
//            if (CtrlID == LBL_C) //If desired control
//            {
//                HDC hdcStatic = (HDC) wParam;
//                SetTextColor(hdcStatic, RGB(0,0,0));
//                SetBkColor(hdcStatic, RGB(230,230,230));
//                return (INT_PTR)hBrush;
//            }
            //HDC hdcStatic = (LBL_C) wParam;
            //SetTextColor(LBL_C, RGB(0,0,0));
            //SetBkMode (LBL_C, TRANSPARENT);
            //return (LRESULT)GetStockObject(NULL_BRUSH);
//        }
		case WM_CLOSE:
        	DestroyWindow(hwnd);
      		break;
		case WM_DESTROY: {
			PostQuitMessage(0);
			break;
		}

		/* All other messages (a lot of them) are processed using default procedures */
		default:
			return DefWindowProc(hwnd, Message, wParam, lParam);
	}
	return 0;
}

void Marker(LONG x, LONG y, HWND hwnd)
{
    HDC hdc;

    hdc = GetDC(hwnd);
        MoveToEx(hdc, (int) x, (int) y, (LPPOINT) NULL);
        LineTo(hdc, (int) x+200, (int) y);
//        MoveToEx(hdc, (int) x, (int) y - 10, (LPPOINT) NULL);
//        LineTo(hdc, (int) x, (int) y + 10);

    ReleaseDC(hwnd, hdc);
}

/* The 'main' function of Win32 GUI programs: this is where execution starts */
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow) {
	WNDCLASSEX wc; /* A properties struct of our window */
	HWND hwnd; /* A 'HANDLE', hence the H, or a pointer to our window */
	MSG msg; /* A temporary location for all messages */

	/* zero out the struct and set the stuff we want to modify */
	memset(&wc,0,sizeof(wc));
	wc.cbSize		 = sizeof(WNDCLASSEX);
	wc.lpfnWndProc	 = WndProc; /* This is where we will send messages to */
	wc.hInstance	 = hInstance;
	wc.hCursor		 = LoadCursor(NULL, IDC_ARROW);
	wc.style		 = CS_HREDRAW | CS_VREDRAW;
	/* White, COLOR_WINDOW is just a #define for a system color, try Ctrl+Clicking it */
	wc.hbrBackground = CreateSolidBrush(0x00f5f5f5);
	//wc.hbrBackground = (HBRUSH)(COLOR_WINDOW);
	wc.lpszClassName = "WindowClass";
	wc.hIcon		 = LoadIcon(NULL, IDI_APPLICATION); /* Load a standard icon */
	wc.hIconSm		 = LoadIcon(NULL, IDI_APPLICATION); /* use the name "A" to use the project icon */

	if(!RegisterClassEx(&wc)) {
		MessageBox(NULL, "Window Registration Failed!","Error!",MB_ICONEXCLAMATION|MB_OK);
		return 0;
	}
	int b= 50; int a = 70;
	hwnd = CreateWindow("WindowClass","Viatori PoS",WS_VISIBLE | WS_OVERLAPPED | WS_CAPTION, CW_USEDEFAULT, CW_USEDEFAULT, 4*a + 5, 6 * b + 84 + 1, NULL,NULL,hInstance,NULL);
    //SetWindowPos(hwnd,0, CW_USEDEFAULT, CW_USEDEFAULT, 4*a + 5, 2*b, 0);
//	LONG lExStyle = GetWindowLong(hwnd, GWL_EXSTYLE);
//	lExStyle &= ~(WS_EX_DLGMODALFRAME | WS_EX_CLIENTEDGE | WS_EX_STATICEDGE);
//	SetWindowLong(hwnd, GWL_EXSTYLE, lExStyle);

	//SetWindowLong(hwnd, GWL_STYLE, 0);
	ShowWindow(hwnd, SW_SHOW);

	//SetWindowLong(hwnd, GWL_EXSTYLE, 0);
	//hwnd = CreateWindow("WindowClass","Caption", WS_VISIBLE|WS_OVERLAPPEDWINDOW,	CW_USEDEFAULT, CW_USEDEFAULT, 360, 467, NULL,NULL,hInstance,NULL);

	//SetDlgItemText

    hLACode = CreateWindow(TEXT("STATIC"), TEXT("KOD"),   WS_CHILD | WS_VISIBLE | WS_TABSTOP, 20 ,3, 30, 20,   hwnd, (HMENU)LBL_C, GetModuleHandle(NULL), NULL);
    hLAAmount = CreateWindow(TEXT("STATIC"), TEXT("TUTAR"),   WS_CHILD | WS_VISIBLE | WS_TABSTOP, 140 ,3, 46, 20,   hwnd, (HMENU)LBL_C, GetModuleHandle(NULL), NULL);
	hTECode   = CreateWindow(TEXT("EDIT"), TEXT(""),  BS_FLAT |WS_CHILD|WS_VISIBLE|ES_AUTOHSCROLL|WS_TABSTOP|ES_RIGHT, (b+10)/4 ,(b-10)/2-2, 2*b, (b-10)/2,   hwnd, (HMENU)EDT_C, GetModuleHandle(NULL), NULL);
	hTEAmount = CreateWindow(TEXT("EDIT"), TEXT(""),  BS_FLAT |WS_CHILD|WS_VISIBLE|ES_AUTOHSCROLL|WS_TABSTOP|ES_RIGHT, (5*b+10)/2, (b-10)/2-2, 2*b, (b-10)/2, hwnd, (HMENU)EDT_A, GetModuleHandle(NULL), NULL);

    hMinMax = CreateWindow(TEXT("BUTTON"),TEXT("M"),  BS_FLAT | WS_TABSTOP | WS_VISIBLE | WS_CHILD, 5 * b - 10, (b - 10 ) / 2 - 2 , b/2, b/2, hwnd, (HMENU)BTN_M,  GetModuleHandle(NULL), NULL);
	hLett = CreateWindow(TEXT("BUTTON"),TEXT("ABC"),  BS_FLAT | WS_TABSTOP | WS_VISIBLE | WS_CHILD, 0, (b+10), a, b, hwnd, (HMENU)BTN_L,  GetModuleHandle(NULL), NULL);
	hBack = CreateWindow(TEXT("BUTTON"),TEXT("Sil"), BS_FLAT |  WS_TABSTOP | WS_VISIBLE | WS_CHILD, a, (b+10), 2*a, b, hwnd, (HMENU)BTN_B,  GetModuleHandle(NULL), NULL);

	hNum1 = CreateWindow(TEXT("BUTTON"),TEXT("1"), BS_FLAT |  WS_TABSTOP | WS_VISIBLE | WS_CHILD,   0, (2*b+10), a, b, hwnd, (HMENU)BTN_1,  GetModuleHandle(NULL), NULL);
	hNum2 = CreateWindow(TEXT("BUTTON"),TEXT("2"), BS_FLAT |  WS_TABSTOP | WS_VISIBLE | WS_CHILD,   a, (2*b+10), a, b, hwnd, (HMENU)BTN_2,  GetModuleHandle(NULL), NULL);
	hNum3 = CreateWindow(TEXT("BUTTON"),TEXT("3"), BS_FLAT |  WS_TABSTOP | WS_VISIBLE | WS_CHILD, 2*a, (2*b+10), a, b, hwnd, (HMENU)BTN_3,  GetModuleHandle(NULL), NULL);

	hNum4 = CreateWindow(TEXT("BUTTON"),TEXT("4"), BS_FLAT | WS_TABSTOP | WS_VISIBLE | WS_CHILD,   0, (3*b+10), a, b, hwnd, (HMENU)BTN_4,  GetModuleHandle(NULL), NULL);
	hNum5 = CreateWindow(TEXT("BUTTON"),TEXT("5"), BS_FLAT | WS_TABSTOP | WS_VISIBLE | WS_CHILD,   a, (3*b+10), a, b, hwnd, (HMENU)BTN_5,  GetModuleHandle(NULL), NULL);
	hNum6 = CreateWindow(TEXT("BUTTON"),TEXT("6"), BS_FLAT |  WS_TABSTOP | WS_VISIBLE | WS_CHILD, 2*a, (3*b+10), a, b, hwnd, (HMENU)BTN_6,  GetModuleHandle(NULL), NULL);

	hNum7 = CreateWindow(TEXT("BUTTON"),TEXT("7"), BS_FLAT |  WS_TABSTOP | WS_VISIBLE | WS_CHILD,   0, (4*b+10), a, b, hwnd, (HMENU)BTN_7,  GetModuleHandle(NULL), NULL);
	hNum8 = CreateWindow(TEXT("BUTTON"),TEXT("8"), BS_FLAT |  WS_TABSTOP | WS_VISIBLE | WS_CHILD,   a, (4*b+10), a, b, hwnd, (HMENU)BTN_8,  GetModuleHandle(NULL), NULL);
	hNum9 = CreateWindow(TEXT("BUTTON"),TEXT("9"), BS_FLAT |  WS_TABSTOP | WS_VISIBLE | WS_CHILD, 2*a, (4*b+10), a, b, hwnd, (HMENU)BTN_9,  GetModuleHandle(NULL), NULL);

	hNum0  = CreateWindow(TEXT("BUTTON"),TEXT("0"),  BS_FLAT | WS_TABSTOP | WS_VISIBLE | WS_CHILD,   0, (5*b+10), a, b, hwnd, (HMENU)BTN_0,  GetModuleHandle(NULL), NULL);
	hNum00 = CreateWindow(TEXT("BUTTON"),TEXT("00"), BS_FLAT |WS_TABSTOP  | WS_VISIBLE | WS_CHILD,   a, (5*b+10), a, b, hwnd, (HMENU)BTN_00, GetModuleHandle(NULL), NULL);
	hDot   = CreateWindow(TEXT("BUTTON"),TEXT("."),  BS_FLAT | WS_TABSTOP | WS_VISIBLE | WS_CHILD, 2*a, (5*b+10), a, b, hwnd, (HMENU)BTN_D,  GetModuleHandle(NULL), NULL);

	hCancel = CreateWindow(TEXT("BUTTON"),TEXT("Iptal"), BS_FLAT |  WS_TABSTOP | WS_VISIBLE | WS_CHILD, 3*a, b+10, a, 5*b/2,       hwnd, (HMENU)BTN_C,  GetModuleHandle(NULL), NULL);
	hSend = CreateWindow(TEXT("BUTTON"),TEXT("Gonder"), BS_FLAT |  WS_TABSTOP | WS_VISIBLE | WS_CHILD,  3*a, 7*b/2 + 10, a, 5*b/2, hwnd, (HMENU)BTN_S,  GetModuleHandle(NULL), NULL);
    hWhole = CreateWindow(TEXT("BUTTON"),TEXT("Kullanici Kontrolu"), BS_FLAT |  WS_TABSTOP | WS_VISIBLE | WS_CHILD,  0, 6 * b + 10 , 4 * a, b, hwnd, (HMENU)BTN_W,  GetModuleHandle(NULL), NULL);
	//hCancel = CreateWindow(TEXT("BUTTON"),TEXT("Iptal"),  WS_TABSTOP | WS_VISIBLE | WS_CHILD, 2*a, (11*b+20)/2, a, b,hwnd, (HMENU)BTN_C,  GetModuleHandle(NULL), NULL);
    EnableWindow(hWhole,false);
    EnableWindow(hMinMax,false);
//    SetBkMode(hLACode, TRANSPARENT);
	SendMessage(hTECode, EM_SETLIMITTEXT, 6, 0);
	SendMessage(hTEAmount, EM_SETLIMITTEXT, 7, 0);
	Marker(50,400, hwnd);
	if(hwnd == NULL) {
		MessageBox(NULL, "Window Creation Failed!","Error!",MB_ICONEXCLAMATION|MB_OK);
		return 0;
	}

	/*
		This is the heart of our program where all input is processed and
		sent to WndProc. Note that GetMessage blocks code flow until it receives something, so
		this loop will not produce unreasonably high CPU usage
	*/
	while(GetMessage(&msg, NULL, 0, 0) > 0) { /* If no error is received... */
		TranslateMessage(&msg); /* Translate key codes to chars if present */
		DispatchMessage(&msg); /* Send it to WndProc */
	}
	return msg.wParam;
}
